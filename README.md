Repo for strangebrew.org website.

Site is built with Hugo, docs here: http://gohugo.io/documentation/

To test with local hugo server, comment out SSL redirect at themes/cocoa/layouts/partials/head_includes.html

add a new page:
- hugo new blog/my-new-post.md
- edit my-new-post.md
- run "hugo server -D" to see what it looks like

Markdown reference: http://commonmark.org/help/
