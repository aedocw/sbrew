---
title: "Calendar"
date: 2023-01-23T16:06:25-07:00
draft: False
---

Email <a href="mailto:strangebrewhbc@gmail.com">strangebrewhbc@gmail.com</a> to add or correct an event.

<iframe src="https://calendar.google.com/calendar/embed?height=600&wkst=1&bgcolor=%23ffffff&ctz=America%2FLos_Angeles&showCalendars=0&title=Beer%20and%20Wine%20Events&src=c3RyYW5nZWJyZXdoYmNAZ21haWwuY29t&color=%23039BE5" style="border:solid 1px #777" width="800" height="600" frameborder="0" scrolling="no"></iframe>

